﻿using OpenTK;

namespace RayTracer.Basic
{
    class Ray
    {
        public Vector3d Origin { get; set; }
        public Vector3d Direction { get; set; }

        public Ray()
        {
            Origin = new Vector3d();
            Direction = Vector3d.UnitZ;
        }

        public Ray(Vector3d origin)
        {
            Origin = origin;
            Direction = Vector3d.UnitZ;
        }

        public Ray(Vector3d origin, Vector3d direction)
        {
            Origin = origin;
            Direction = direction;
        }

        public Vector3d GetPoint(double distance)
        {
            return Direction*distance;
        }
    }
}
