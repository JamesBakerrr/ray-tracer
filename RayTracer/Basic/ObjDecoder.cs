﻿using System;
using System.Collections.Generic;
using OpenTK;
using RayTracer.Primitives;

namespace RayTracer.Basic
{
    internal static class ObjDecoder
    {
        public static IList<Triangle> TrianglesFromFile(string file)
        {
            var verts = new List<Vector3d>();
            var faces = new List<Vector3d>();
            var triangles = new List<Triangle>();

            var lines = file.Split(new[] {'\n'});

            foreach (var line in lines)
            {
                var splitLine = line.Split(new[] {' '});

                switch (splitLine[0])
                {
                    case "v":   verts.Add(GetVert(splitLine)); break;
                    case "f":   faces.Add(GetFace(splitLine)); break;
                    default:
                        Console.WriteLine(Resources.OBJUnsupported, splitLine[0]);
                        break;
                }
            }

            foreach (var face in faces)
            {
                var p0 = verts[(int) face.X];
                var p1 = verts[(int) face.Y];
                var p2 = verts[(int) face.Z];

                triangles.Add(new Triangle(p0, p1, p2, null));
            }



            return triangles;
        }

        private static Vector3d GetVert(IList<string> args)
        {
            if (args.Count != 4)
                throw new ArgumentException();

            return new Vector3d(Double.Parse(args[1]),
                                Double.Parse(args[2]),
                                Double.Parse(args[3]));
        }

        private static Vector3d GetFace(IList<string> args)
        {
            if (args.Count != 4)
                throw new ArgumentException();

            return new Vector3d(int.Parse(args[1]),
                                int.Parse(args[2]),
                                int.Parse(args[3]));
        }
    }
}

/*
namespace RayTracer.Basic
{
    internal static class ObjFileDecoder
    {
        public static Triangle[] TrianglesFromFile(string file, Vector3d position, Vector3d scale, Vector3d rotation, Material material)
        {
            var vertices = new List<Vector3d>();
            var faces = new List<int[]>();
            var triangles = new List<Triangle>();
            var separators = new char[] {' '};

            var xrot = rotation.X*Math.PI/180;
            var yrot = rotation.Y*Math.PI/180;
            var zrot = rotation.Z*Math.PI/180;

            foreach (var line in file.Split(new char[] { '\n' }, StringSplitOptions.None))
            {
                var splitLine = line.Trim().Split(separators, 4);

                if (splitLine[0] == "v")
                {
                    var vert = new Vector3d(Double.Parse(splitLine[1]), Double.Parse(splitLine[2]), Double.Parse(splitLine[3]));

                    vert = Vector3d.Multiply(vert, scale);

                    vert = new Vector3d(1, vert.Y*Math.Cos(xrot) - vert.Z*Math.Sin(xrot),
                                        vert.Y*Math.Sin(xrot) + vert.Z*Math.Cos(xrot));
                    vert = new Vector3d(vert.Z*Math.Cos(yrot) - vert.X*Math.Sin(yrot), 1,
                                        vert.Z*Math.Sin(yrot) + vert.X*Math.Cos(xrot));
                    vert = new Vector3d(vert.X*Math.Cos(zrot) - vert.Y*Math.Sin(zrot),
                                        vert.X*Math.Sin(zrot) + vert.Y*Math.Cos(zrot), 1);

                    vert += position;

                    vertices.Add(vert);
                }
                else if(splitLine[0] == "f")
                {
                    faces.Add(new int[]
                                  {
                                      int.Parse(splitLine[1]), 
                                      int.Parse(splitLine[2]), 
                                      int.Parse(splitLine[3])
                                  });
                }

                
            }
            
            foreach (var face in faces)
            {
                triangles.Add(new Triangle(vertices[face[0] - 1], vertices[face[1] - 1], vertices[face[2] - 1], material));
            }

            Console.WriteLine("Loaded obj");

            return triangles.ToArray();
        }
    }
}
*/