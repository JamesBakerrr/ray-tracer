﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenTK.Graphics;

namespace RayTracer
{
    internal static class ColorExtensions
    {
        /*
         * Alpha component has been 'disabled' because it is not used 
         * in the ray-tracer but causes a performance decrease.
         */

        public static void Add(ref Color4 a, ref Color4 b)
        {
            //a.A += b.A;
            a.B += b.B;
            a.R += b.R;
            a.G += b.G;
        }

        public static void Add(ref Color4 a, Color4 b)
        {
            //a.A += b.A;
            a.B += b.B;
            a.R += b.R;
            a.G += b.G;
        }

        public static void Multiply(ref Color4 a, ref Color4 b)
        {
            //a.A *= b.A;
            a.B *= b.B;
            a.R *= b.R;
            a.G *= b.G;
        }

        public static void Multiply(ref Color4 a, Color4 b)
        {
            //a.A *= b.A;
            a.B *= b.B;
            a.R *= b.R;
            a.G *= b.G;
        }

        public static void Multiply(ref Color4 a, float b)
        {
            //a.A *= b;
            a.B *= b;
            a.R *= b;
            a.G *= b;
        }

        public static void Clamp(ref Color4 a)
        {
            //a.A = Math.Min(a.A, 1);
            a.R = Math.Min(a.R, 1);
            a.G = Math.Min(a.G, 1);
            a.B = Math.Min(a.B, 1);
        }
    }
}
