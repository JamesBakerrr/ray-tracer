﻿using System;

using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;


namespace RayTracer
{
    internal class RayTracer : GameWindow
    {
        private readonly int _width;
        private readonly int _height;
        private readonly int[] _image;

        private uint _texture;
        private int _vbo, _ebo;
        private int _shaderProgram;

        private static readonly float[] ImageBounds = new[]
            {
                -1.0f, 1.0f, 0.0f, 0.0f,
                1.0f, 1.0f, 1.0f, 0.0f,
                1.0f, -1.0f, 1.0f, 1.0f,
                -1.0f, -1.0f, 0.0f, 1.0f
            };

        private static readonly int[] Elements = new[]
            {
                0, 1, 2,
                2, 3, 0
            };

        public RayTracer(int width, int height) : 
               base(width, height, GraphicsMode.Default, "Ray Tracer")
        {
            _width = width;
            _height = height;
            _image = new int[_width * _height];

            WindowBorder = WindowBorder.Fixed;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            GL.ClearColor(0.0f, 0.0f, 0.0f, 1.0f);

            SetupBuffers();
            SetupShader();
            SetupAttributes();
            SetupTextures();

            RayTrace();
        }

        protected override void OnUnload(EventArgs e)
        {
            GL.DeleteTexture(_texture);
            GL.DeleteProgram(_shaderProgram);
            GL.DeleteBuffers(2, new[] {_vbo, _ebo});
            
            base.OnUnload(e);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.UseProgram(_shaderProgram);
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);
            GL.BindTexture(TextureTarget.Texture2D, _texture);

            GL.DrawElements(BeginMode.Triangles, 6, DrawElementsType.UnsignedInt, 0);

            SwapBuffers();
        }

        private void SetupBuffers()
        {
            //Vertex Buffer Object
            GL.GenBuffers(1, out _vbo);
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferData(BufferTarget.ArrayBuffer,
                          (IntPtr)(sizeof(float) * ImageBounds.Length),
                          ImageBounds, BufferUsageHint.StaticDraw);

            //Element Buffer Object
            GL.GenBuffers(1, out _ebo);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer,
                          (IntPtr)(sizeof(int) * Elements.Length),
                          Elements, BufferUsageHint.StaticDraw);
        }

        private void SetupShader()
        {
            //Create vertex shader
            int vertHandler = GL.CreateShader(ShaderType.VertexShader);
            GL.ShaderSource(vertHandler, Resources.Vert);
            GL.CompileShader(vertHandler);

            //Create fragment shader
            int fragHandler = GL.CreateShader(ShaderType.FragmentShader);
            GL.ShaderSource(fragHandler, Resources.Frag);
            GL.CompileShader(fragHandler);

            //Log the status of the shaders
            Console.WriteLine("Vertex Shader: {0}", GL.GetShaderInfoLog(vertHandler));
            Console.WriteLine("Fragment Shader: {0}", GL.GetShaderInfoLog(fragHandler));

            //Create, link, and use program
            _shaderProgram = GL.CreateProgram();
            GL.AttachShader(_shaderProgram, vertHandler);
            GL.AttachShader(_shaderProgram, fragHandler);
            GL.BindFragDataLocation(_shaderProgram, 0, "outColor");
            GL.LinkProgram(_shaderProgram);
            GL.UseProgram(_shaderProgram);
        }

        private void SetupAttributes()
        {
            //Position Attribute
            int posAttrib = GL.GetAttribLocation(_shaderProgram, "position");
            GL.VertexAttribPointer(posAttrib, 2,
                                   VertexAttribPointerType.Float,
                                   false, sizeof (float)*4, 0);
            GL.EnableVertexAttribArray(posAttrib);

            //Texture Coordinates Attribute
            int texAttrib = GL.GetAttribLocation(_shaderProgram, "texcoord");
            GL.EnableVertexAttribArray(texAttrib);
            GL.VertexAttribPointer(texAttrib, 2,
                                   VertexAttribPointerType.Float,
                                   false, sizeof (float)*4,
                                   sizeof (float)*2);
        }

        private void SetupTextures()
        {
            GL.GenTextures(1, out _texture);
            GL.BindTexture(TextureTarget.Texture2D, _texture);

            GL.TexParameter(TextureTarget.Texture2D,
                            TextureParameterName.TextureMinFilter,
                            (int)TextureMinFilter.Linear);

            GL.TexParameter(TextureTarget.Texture2D,
                            TextureParameterName.TextureMagFilter,
                            (int)TextureMagFilter.Linear);
        }

        private void RayTrace()
        {
            var scene = Scene.TestScene();

            scene.Camera.GetImage(_image);

            GL.BindTexture(TextureTarget.Texture2D, _texture);
            GL.TexImage2D(TextureTarget.Texture2D, 0, 
                          PixelInternalFormat.Rgba, _width, _height, 0,
                          PixelFormat.Bgra, PixelType.UnsignedByte, 
                          _image);
        }

        [STAThread]
        static void Main()
        {
            using (var tracer = new RayTracer(800, 600))
            {
                tracer.Run();
            }
        }
    }
}
