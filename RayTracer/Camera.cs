﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
//using System.Threading.Tasks;
using System.Threading.Tasks;
using OpenTK;
using RayTracer.Basic;
using RayTracer.Primitives;
using OpenTK.Graphics;

namespace RayTracer
{
    internal class Camera
    {
        public Scene Scene { get; private set; }

        public Vector3d Origin { get; private set; }
        public Vector3d LookAt { get; private set; }
        public double FOV { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        public uint Samples { get; private set; }

        private readonly Vector3d _u, _v, _w, _planeMid;
        private readonly Stopwatch _timer = new Stopwatch();
        private readonly double _fovx, _fovy;
        private double _lastRefraction;

        private readonly float _sampleLength;
        private readonly float _sampleContribution;

        private const double epsilon = 1e-4;

        public Camera(Scene scene, Vector3d origin, Vector3d lookAt, int width,
                      int height, double fov = 75.0, uint samples = 4)
        {
            Scene = scene;

            LookAt = Vector3d.Normalize(lookAt);
            FOV = fov;
            Width = width;
            Height = height;
            Samples = samples < 2 ? 2 : samples;
            Origin = origin;

            _sampleLength = 1.0f/(Samples/2.0f);
            _sampleContribution = 4.0f/(Samples*Samples);

            var fovRad = fov*Math.PI/180;

            if (Math.Abs(fovRad) > epsilon)
            {
                _fovx = fovRad;
                _fovy = 2*Math.Atan(Math.Tan(_fovx/2)*height/width);
            }

            //Unit vector in the direction of LookAt
            _w = Vector3d.Normalize(-LookAt);

            //Unit vector of X axis of screen relative to world
            _u = -Vector3d.Normalize(Vector3d.Cross(Vector3d.UnitY, -LookAt));

            //Unit vector of Y axis of screen relative to world
            _v = Vector3d.Cross(_w, _u);
            
            //Account for x-FOV
            _u *= Math.Tan(_fovx/2)/Width;

            //Account for y-FOV
            _v *= Math.Tan(_fovy/2)/Height;


        }

        public void GetImage(int[] image)
        {
            _timer.Start();

            var halfWidth = Width/2;
            var halfHeight = Height/2;

            Parallel.For(0, Height, y =>
            {
                int pixel = y * Width;
                var hitRay = new Ray(Origin, -_w);

                for (int x = 0; x < Width; x++)
                {
                    var result = new Color4();

                    for (float sx = 0.0f; sx < 1.0f; sx += _sampleLength)
                    {
                        for (float sy = 0.0f; sy < 1.0f; sy += _sampleLength)
                        {
                            var viewDir =  LookAt + (x - halfWidth + sx)*_u + (y - halfHeight + sy)*_v;

                            hitRay.Direction = Vector3d.Normalize(viewDir);

                            _lastRefraction = 1.0;
                            ColorExtensions.Add(ref result, CalculateRadiance(hitRay));
                        }
                    }

                    ColorExtensions.Multiply(ref result, _sampleContribution);
                    ColorExtensions.Clamp(ref result);
                    image[pixel++] = result.ToArgb();
                }
            });
                
            _timer.Stop();
            Console.WriteLine(Resources.RenderTime, _timer.ElapsedMilliseconds);
        }

        private Color4 CalculateRadiance(Ray ray, int depth = 0, int maxDepth = 5)
        {
            double hitLen;
            var hitObj = Scene.IntersectRay(ray, out hitLen);

            if (hitObj == null || depth > maxDepth)
            {
                return Color4.Black;
            }

            var hitPoint = ray.Origin + ray.Direction * hitLen;

            var normal = hitObj.GetNormal(hitPoint);

            var color = hitObj.Material.CalculateColor(Scene, hitPoint, normal);
            
            if (hitObj.Material.Reflection > 0.0f)
            {
                var reflectionDirection = normal*(2.0f*Vector3d.Dot(-ray.Direction, normal)) + ray.Direction;

                var reflectedRay = new Ray(hitPoint, reflectionDirection);

                var reflection = CalculateRadiance(reflectedRay, depth + 1, maxDepth);

                ColorExtensions.Multiply(ref reflection, hitObj.Material.Reflection);
                ColorExtensions.Add(ref color, ref reflection);
            }
                
            if (hitObj.Material.Transparency > 0.0f)
            {
                var refractRatio = _lastRefraction/hitObj.Material.Refraction;
                var c1 = -Vector3d.Dot(normal, ray.Direction);
                var c2 = Math.Sqrt(1 - refractRatio*refractRatio*(1 - c1*c1));

                var refractionDirection = (refractRatio*ray.Direction) + (refractRatio*c1 - c2)*normal;
                var refractionRay = new Ray(hitPoint, refractionDirection);

                var refraction = CalculateRadiance(refractionRay, depth + 1, maxDepth);
                    
                ColorExtensions.Multiply(ref refraction, hitObj.Material.Transparency);
                ColorExtensions.Add(ref color, ref refraction);
            }

            _lastRefraction = hitObj.Material.Refraction;

            if (Math.Abs(_lastRefraction) < epsilon)
            {
                _lastRefraction = 1.0f;
            }

            return color;
        }
    }
}
