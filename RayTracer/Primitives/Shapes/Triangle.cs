﻿using System;

using OpenTK;
using RayTracer.Basic;

namespace RayTracer.Primitives
{
    internal class Triangle : IPrimitive
    {
        private const double epsilon = 1e-4;

        public Material Material { get; set; }
        public Vector3d[] Points { get; private set; }
        private Vector3d _normal;

        public Triangle(Vector3d p0, Vector3d p1, Vector3d p2, Material material)
        {
            Material = material;
            Points = new[] {p0, p1, p2};

            _normal = Vector3d.Normalize(Vector3d.Cross(p2 - p0, p1 - p0));
        }

        /*
         * Barycentric intersection
         *  - http://www.cs.virginia.edu/~gfx/Courses/2003/ImageSynthesis/papers/Acceleration/Fast%20MinimumStorage%20RayTriangle%20Intersection.pdf
         *  
         * A point T(u, v) on a triangle is given by
         *  T(u, v) = (1-u-v)*p0 + u*p1 + v*p2 
         *  where (u, v) are the barycentric coordinates which must fullfill
         *  u >= 0, v >= 0 and u + v <= 1   
         *  
         * (u,v) can be used for texture mapping, normal interpolation, etc
         * */

        public double GetIntersection(Ray ray)
        {
            // find edges sharing p0
            var edge1 = Points[1] - Points[0];
            var edge2 = Points[2] - Points[0];

            var p = Vector3d.Cross(ray.Direction, edge2);

            var determinant = Vector3d.Dot(edge1, p);

            if (Math.Abs(determinant) < epsilon)
                return -1;

            var invDeterminant = 1/determinant;

            var tvec = ray.Origin - Points[0];

            // calculate U parameter and test bounds 
            var u = invDeterminant*Vector3d.Dot(tvec, p);

            if (u < 0 || u > 1)
                return -1;

            var q = Vector3d.Cross(tvec, edge1);

            // calculate V parameter and test bounds
            var v = invDeterminant*Vector3d.Dot(ray.Direction, q);
            if (v < 0 || u + v > 1)
                return -1;

            var t = invDeterminant*Vector3d.Dot(edge2, q);

            if (t > epsilon) // ray intersection
                return t;

            // line but not ray intersection
            return -1;
        }

        public Vector3d GetNormal(Vector3d point)
        {
            return _normal;
        }

        public Boolean PointInTriangle(Vector3d point)
        {
            var v0 = Points[2] - Points[0];
            var v1 = Points[1] - Points[0];
            var v2 = point - Points[0];

            var dot00 = Vector3d.Dot(v0, v0);
            var dot01 = Vector3d.Dot(v0, v1);
            var dot02 = Vector3d.Dot(v0, v2);
            var dot11 = Vector3d.Dot(v1, v1);
            var dot12 = Vector3d.Dot(v1, v2);

            var denom = 1 / (dot00 * dot11 - dot01 * dot01);
            var u = (dot11 * dot02 - dot01 * dot12) * denom;
            var v = (dot00 * dot12 - dot01 * dot02) * denom;

            return u >= 0 && v >= 0 && u + v < 1;
        }
    }
} 
