﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Threading.Tasks;

using OpenTK;
using RayTracer.Basic;

namespace RayTracer.Primitives.Shapes
{
    class Mesh : IPrimitive
    {
        public Vector3d Origin { get; private set; }
        public Vector3d Scale { get; private set; }
        public Vector3d Rotation { get; private set; }
        public Material Material { get; private set; }
        

        private readonly IList<Triangle> _triangles;
        private Triangle _lastTriangle;

        public Mesh(Vector3d origin, Vector3d scale, Vector3d rotation, Material material, string file)
        {
            Origin = origin;
            Scale = scale;
            Rotation = rotation;
            Material = material;

            _triangles = ObjDecoder.TrianglesFromFile(file);
            TransformMesh(origin, scale, rotation, material);
        }

        private void TransformMesh(Vector3d origin, Vector3d scale, Vector3d rotation, Material material)
        {
            foreach (var triangle in _triangles)
            {
                //TODO: things
                //TODO: scaleitize, rotatize

                triangle.Points[0] += origin;
                triangle.Points[1] += origin;
                triangle.Points[2] += origin;

                triangle.Material = material;
            }
        }

        public double GetIntersection(Ray ray)
        {
            var hitLen = double.PositiveInfinity;
            foreach (var triangle in _triangles)
            {
                var t = triangle.GetIntersection(ray);

                if (t > 0 && t < hitLen)
                {
                    hitLen = t;
                    _lastTriangle = triangle;
                }

            }

            return hitLen;
        }

        public Vector3d GetNormal(Vector3d point)
        {
            /*foreach (var triangle in _triangles)
            {
                if(triangle.PointInTriangle(point))
                {
                    return triangle.GetNormal(point);
                }
            }*/

            //return _triangles[0].GetNormal(point);

            //Point isn't on mesh, so the normal doesn't exist
            //throw new Exception("Point not in mesh");

            return _lastTriangle.GetNormal(point);
        }
    }
}
