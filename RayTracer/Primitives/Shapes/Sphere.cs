﻿using System;

using OpenTK;
using RayTracer.Basic;

namespace RayTracer.Primitives
{
    class Sphere : IPrimitive
    {
        private const double epsilon = 1e-4;

        public Material Material { get; private set; }
        public Vector3d Origin { get; private set; }
        public double Radius { get; private set; }

        public Sphere(Vector3d origin, double radius, Material material)
        {
            Material = material;
            Origin = origin;
            Radius = radius;
        }

        public double GetIntersection(Ray ray)
        {
            Vector3d diff = Origin - ray.Origin;

            double b = Vector3d.Dot(diff, ray.Direction);
            double c = Vector3d.Dot(diff, diff) - Radius*Radius;

            double determinant = b*b - c; // a = 1 because ray is normalized

            if (determinant < 0)
            {
                return -1.0;
            }

            determinant = Math.Sqrt(determinant);

            var tmp = b - determinant;

            // Math.Min(b - det, b + det);
            // don't calculate both values unless necessary
            var minimumDistance = b - determinant > epsilon ? tmp : b + determinant;  

            return minimumDistance > epsilon ? minimumDistance : -1.0;
        }

        public Vector3d GetNormal(Vector3d point)
        {
            return (point - Origin) / Radius;
        }
    }
}
