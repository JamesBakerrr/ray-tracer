﻿using OpenTK;
using RayTracer.Basic;

namespace RayTracer.Primitives
{
    class Plane : IPrimitive
    {
        public Material Material { get; private set; }
        public Vector3d Origin { get; private set; }
        public Vector3d Normal { get; private set; }

        public Plane(Vector3d origin, Vector3d normal, 
                     Material material)
        {
            Material = material;
            Origin = origin;
            Normal = normal;
        }


        public double GetIntersection(Ray ray)
        {
            var num = Vector3d.Dot(Origin - ray.Origin, Normal);
            var den = Vector3d.Dot(ray.Direction, Normal);

            if ((num > 0 && den > 0) || (num < 0 && den < 0))
            {
                return num/den;
            }

            return -1.0;
        }

        public Vector3d GetNormal(Vector3d point)
        {
            return Normal;
        }
    }
}
