﻿using OpenTK;
using OpenTK.Graphics;

namespace RayTracer.Primitives
{
    class Material
    {
        public Color4 Color { get; set; }

        public float Reflection { get; set; }
        public float Transparency { get; set; }
        public float Refraction { get; set; }

        public virtual Color4 CalculateColor(Scene scene, Vector3d point, Vector3d normal)
        {
            return Color;
        }
    }
}
