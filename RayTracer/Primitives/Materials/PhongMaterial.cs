﻿using System;
using System.Collections.Generic;

using OpenTK;
using OpenTK.Graphics;
using RayTracer.Basic;

namespace RayTracer.Primitives
{
    internal class PhongMaterial : Material
    {
        public float Diffuse { get; set; }

        /// <summary>
        /// Specular reflection factor
        /// </summary>
        public float SpecularReflection { get; set; }

        public float Shininess { get; set; }

        public override Color4 CalculateColor(Scene scene, Vector3d point, Vector3d normal)
        {
            var diffuse = new Color4();
            var specular = new Color4();

            if (scene.Lights.Count > 0)
            {
                foreach (var light in scene.Lights)
                {
                    var lightDir = Vector3d.Normalize(light.Origin - point);
                    var distance = Math.Max((light.Origin - point).Length - light.Radius, 0);

                    var shadowRay = new Ray(point, lightDir);

                    double shadowHitDistance;
                    var shadowHit = scene.IntersectRay(shadowRay, out shadowHitDistance);

                    if (shadowHit != null && shadowHitDistance <= distance)
                        continue;
                    
                    var diffLevel = Vector3d.Dot(lightDir, normal);
                    
                    //Constant, Linear, and Quadratic attenuation factors
                    var factors = distance/light.Radius + 1;
                    var attenuation = 1/(factors*factors);
                    attenuation = (attenuation - light.CutOff)/(1 - light.CutOff);
                    attenuation = Math.Max(attenuation, 0);

                    if (diffLevel > 0)
                    {
                        var tempdiff = light.Color;

                        ColorExtensions.Multiply(ref tempdiff, (float) (diffLevel*Diffuse*attenuation));
                        ColorExtensions.Add(ref diffuse, tempdiff);
                    }

                    var reflectionDir = normal*Vector3d.Dot(normal*2, lightDir) - lightDir;
                    var specularLevel = Vector3d.Dot(reflectionDir, normal);

                    if (specularLevel > 0)
                    {
                        var tempdiff = light.Color;

                        ColorExtensions.Multiply(ref tempdiff, (float) (SpecularReflection*Math.Pow(specularLevel, Shininess)));
                        ColorExtensions.Add(ref specular, tempdiff);
                    }
                }
            }

            var final = Color;

            ColorExtensions.Multiply(ref final, 0.10f);

            ColorExtensions.Add(ref final, diffuse);
            ColorExtensions.Add(ref final, specular);

            return final;
        }
    }
}
