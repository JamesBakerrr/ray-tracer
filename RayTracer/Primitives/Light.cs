﻿using OpenTK;
using OpenTK.Graphics;

namespace RayTracer.Primitives
{
    class Light
    {
        public Vector3d Origin { get; private set; }
        public double Radius { get; private set; }
        public Color4 Color { get; private set; }
        public double CutOff { get; private set; }

        public Light(Vector3d origin, double radius, Color4 color,
                     double cutoff = 0.005)
        {
            Origin = origin;
            Radius = radius;
            Color = color;
            CutOff = cutoff;
        }
    }
}
