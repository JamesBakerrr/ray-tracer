﻿using OpenTK;
using RayTracer.Basic;

namespace RayTracer.Primitives
{
    interface IPrimitive
    {
        Material Material { get; }
        double GetIntersection(Ray ray);
        Vector3d GetNormal(Vector3d point);
    }
}
