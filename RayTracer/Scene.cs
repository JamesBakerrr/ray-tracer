﻿using System.Collections.Generic;

using OpenTK;
using OpenTK.Graphics;
using RayTracer.Basic;
using RayTracer.Primitives;
using RayTracer.Primitives.Shapes;

namespace RayTracer
{
    class Scene
    {
        public Camera Camera { get; set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        public List<IPrimitive> Objects { get; private set; }
        public List<Light> Lights { get; private set; }

        public Scene(int width, int height)
        {
            Width = width;
            Height = height;

            Objects = new List<IPrimitive>();
            Lights = new List<Light>();
        }

        public IPrimitive IntersectRay(Ray ray, out double hitLen)
        {
            IPrimitive hitObj = null;
            hitLen = double.PositiveInfinity;

            foreach (var obj in Objects)
            {
                var t = obj.GetIntersection(ray);

                if (t <= 0 || t >= hitLen)
                    continue;   

                hitObj = obj;
                hitLen = t;
            }

            return hitObj;
        }

        public static Scene TestScene()
        {
            var scene = new Scene(800, 600);
            
            var redMat = new PhongMaterial
                {
                    Color = Color4.Red,
                    Diffuse = 0.5f,
                    SpecularReflection = 0.5f,
                    Shininess = 100
                };

            var cyanMat = new PhongMaterial
                {
                    Color = Color4.Cyan,
                    Diffuse = 0.5f,
                    Reflection = 0.1f,
                    SpecularReflection = 0.5f,
                    Shininess = 100
                };

            var greenMat = new PhongMaterial
                {
                    Color = Color4.Green,
                    Diffuse = 0.5f,
                    Reflection = 0.5f,
                    SpecularReflection = 0.5f,
                    Shininess = 100
                };

            var orangeMat = new PhongMaterial
                {
                    Color = Color4.Orange,
                    Diffuse = 0.5f,
                    Reflection = 0.5f,
                    SpecularReflection = 0.5f,
                    Shininess = 100
                };

            var refractMat = new PhongMaterial
                {
                    Color = Color4.Blue,
                    Refraction = 1.33f,
                    Transparency = 1.0f,
                };

            var fullbrightMat = new Material()
                {
                    Color = Color4.Blue
                };

            scene.Objects.Add(new Sphere(new Vector3d(100, 0, 200), 100, redMat));
            scene.Objects.Add(new Sphere(new Vector3d(-200, -50, 50), 100, greenMat));
            //scene.Objects.Add(new Sphere(new Vector3d(0, -75, 100), 100, refractMat));

            scene.Objects.Add(new Triangle(new Vector3d(100, 0, 300), new Vector3d(-200, -50, 150), new Vector3d(-50, 100, 200), orangeMat));

            scene.Objects.Add(new Plane(new Vector3d(0, -200, 0), Vector3d.UnitY, cyanMat));
            scene.Objects.Add(new Mesh(new Vector3d(450, 50, 0), new Vector3d(100, 100, 100), new Vector3d(), redMat, Resources.Cube));

            scene.Lights.Add(new Light(new Vector3d(-50, 100, 0), 300, new Color4(255, 255, 90, 255)));
            scene.Lights.Add(new Light(new Vector3d(-300, 1000, 100), 100, new Color4(255, 128, 0, 255)));

            var cameraOrigin = new Vector3d(0, 200, -1500);

            scene.Camera = new Camera(scene, cameraOrigin, Vector3d.Normalize(((Sphere)scene.Objects[0]).Origin - cameraOrigin), scene.Width, scene.Height,
                                      samples:4);
            return scene;
        }

        public static Scene CornellBox()
        {
            var scene = new Scene(800, 600);

            scene.Camera = new Camera(scene, Vector3d.Zero, Vector3d.UnitZ, scene.Width, scene.Height,
                                      samples: 8);

            var redMat = new PhongMaterial
            {
                Color = Color4.Red,
                Diffuse = 0.5f,
            };

            var greenMat = new PhongMaterial
            {
                Color = Color4.Green,
                Diffuse = 0.5f,
            };

            var whiteMat = new PhongMaterial
            {
                Color = Color4.White,
                Diffuse = 0.5f,
                Shininess = 0.1f,
                SpecularReflection = 0.1f,
            };

            var boxMat = new PhongMaterial
            {
                Color = Color4.LightSlateGray,
                Diffuse = 0.5f,
                Reflection = 0.2f,
            };

            scene.Objects.Add(new Mesh(new Vector3d(0, 0, 300), new Vector3d(100, 100, 0), new Vector3d(0, 0, 0),  whiteMat, Resources.Plane));
            scene.Lights.Add(new Light(new Vector3d(0, 100, 50), 100.0, Color4.LightYellow));

            return scene;
        }
    }
}
